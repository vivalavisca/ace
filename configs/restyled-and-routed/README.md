# restyled-and-routed ace profile

## A simple CRA based project file system

### All components are CamelCased and contain:

- index.js (JSX component code)
- styled.js (styled-components)

### Commands

1. create a component's directory and its files  
   `ace c -n ComponentNameHere`
2. create a view's directory and its files  
   `ace v -n ViewNameHere`

### File Structure

- /views
  - /ViewNameHere
    - index.js
    - styled.js
  - /OtherViewNameHere
    - index.js
    - styled.js
- /components
  - /ComponentNameHere
    - index.js
    - styled.js
  - /OtherComponentNameHere
    - index.js
    - styled.js
- /contexts
  - GlobalStore.js
  - Theme.js
  - Language.js
