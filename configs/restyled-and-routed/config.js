module.exports = {
  name: "restyled-and-routed",
  dependencies: ["npx"],
  initSteps: [
    {
      message: "Installing React",
      exec: "npx create-react-app tmp && cd tmp && mv * ../ && cd ../ && touch a-new-file.txt",
    },

    // {
    //   message: "Cleaning up",
    //   exec: "rm ./src/*.test.js ./src/App.* ./src/*.svg ./src/setupTests.js",
    // },
    // {
    //   message: "Generating Replacements for App.js",
    //   exec: `touch ./src/App.js && echo 'import React from "react";' >> ./src/App.js && echo 'const App = () => <div>React st</div>;' >> ./src/App.js && echo 'export default App;' >> ./src/App.js`,
    // },
  ],
  // File system defined in "json-file-structure" format
  srcSkeleton: [
    {
      name: "components",
      children: [],
    },
    {
      name: "views",
      children: [],
    },
    {
      name: "utils",
      children: [],
    },
    {
      name: "contexts",
      children: [],
    },
  ],
  commandTools: [
    {
      command: "component",
      params: ["n"],
      exec: "mkdir ./src/components/%n && touch ./src/components/%n/index.js && touch ./src/components/%n/styled.js",
    },
    {
      command: "view",
      params: ["n"],
      exec: "touch ./src/views/%n.js",
    },
  ],
};
